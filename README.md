# BatteryControlDroid

This is an Android app to notify when battery charge reach percentage set (minimum or maximum)

These are some app screenshots:

Main screen:

![](Screenshot_20230520_085346.png)

![](Screenshot_20230520_085417.png)

Menu:

![](Screenshot_20230520_090253.png)

Screen about:

![](Screenshot_20230520_085431.png)
