package org.oml.batterycontroldroid;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.util.Calendar;

public class AboutActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_html);

        WebView wvAbout = findViewById(R.id.wvAbout);
        wvAbout.setWebViewClient(new WebViewClient());
        // Set the Text Size to fit text on window
        wvAbout.getSettings().setTextZoom(80);
        wvAbout.getSettings().setUseWideViewPort(true);
        wvAbout.getSettings().setLoadWithOverviewMode(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(BuildConfig.VERSION_BUILD_DATE);
        String html = getResources().getString(R.string.message_about_google_html,
                BuildConfig.VERSION_NAME, BuildConfig.VERSION_BUILD, calendar.getTime()
                        .toLocaleString());
        wvAbout.loadData(html, "text/html", "UTF-8");

        /*
         * Open external browser. gitlab.com needs JavaScript.
         */
        wvAbout.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(final WebView view, final String url) {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(i);

                return true;
            }
        });

    }

}