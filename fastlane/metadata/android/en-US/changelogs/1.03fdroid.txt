Added metadata structure for F-Droid
Added distributionSha256Sum to gradle-wrapper.properties.
Removed permissions not required from AndroidManifest.xml.
Fixed error in the if that checks if there is something to notify.
Fixed so that Google voice is disabled if Low Voice is enabled and vice versa.
Fixed public definitions not being necessary to private.